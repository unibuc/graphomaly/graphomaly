=================
Graphomaly Models
=================


Autoencoder
~~~~~~~~~~~

.. automodule:: models.autoencoder
   :members:
   :undoc-members:
   :show-inheritance:


Dictionary Learning
~~~~~~~~~~~~~~~~~~~
.. autoclass:: models.dictlearn.AnomalyDL
   :members:


Isolation Forest
~~~~~~~~~~~~~~~~

.. automodule:: models.sklearn
   :members:
   :undoc-members:
   :show-inheritance:


Variational Autoencoder
~~~~~~~~~~~~~~~~~~~~~~~

.. autoclass:: models.vae.VAE
   :members:
   :undoc-members:
   :show-inheritance:
