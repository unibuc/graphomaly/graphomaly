.. Graphomaly documentation master file, created by
   sphinx-quickstart on Thu Mar 10 22:40:26 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Graphomaly's documentation!
======================================

Graphomaly is a package for anomaly detection in graphs, especially in those
modeling bank transactions. There are many possible applications, among which
detecting money laundering schemes has a main position.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   data_types
   features
   preprocessing
   models
   estimator



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
