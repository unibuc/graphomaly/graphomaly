.. _sec-data-types:

==========
Data types
==========

As its main purpose is to analyze banks transactions,
Graphomaly works with the following types of data.

* Transaction list: a matrix with a row for each transaction.
  Its first three columns must be the source id, destination id and amount.
  Dataframes are also allowed in lower level calls; there, the above three columns
  may have arbitrary positions and names.
  The matrix or dataframe may have other columns, that can also be used for
  feature extraction.
  A `synthetic example <http://graphomaly.upb.ro/Date/L1.zip>`_ can be used
  for simple tests, like in our notebooks.
* Graph: a matrix with a row for each edge, the nodes being clients ids.
  Edges are oriented from the source id to the destination id.
  Each node has at least two attributes: cumulated amount (sum of the amounts
  of all transfers from source to destination) and number of transactions from
  source to destination.
  In certain functions, the graph can be given as a dataframe or a Networkx DiGraph.
  Examples:
  real and large `Libra dataset <http://graphomaly.upb.ro/Date/Libra_bank_3months_graph.zip>`_,
  medium size `synthetic (50000 nodes) <http://graphomaly.upb.ro/Date/G12.zip>`_,
  small size `synthetic (10000 nodes) <http://graphomaly.upb.ro/Date/G1.zip>`_.
  The small example corresponds to the
  `transactions list above <http://graphomaly.upb.ro/Date/L1.zip>`_.
* Features: a matrix with a row for each node, the columns being user selected features
  that will be computed by Graphomaly functions.
  Alternatively, features can be associated with edges.

A client id is a number.
There are some advantages if the ids are numbered successively from zero,
but this is not mandatory.

The amounts can be expressed in the same or different currencies,
but graph features analysis relies on the fact that the amounts are comparable.
Converting to the same currency is user's responsibility.
