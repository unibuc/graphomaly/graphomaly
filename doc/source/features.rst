==================
Feature extraction
==================

Most of the anomaly detection algorithms from Graphomaly build first a set of
features associated with the nodes or the edges of the graph.

.. _sec-graph-to-features:

-----------------
Graph to features
-----------------

Given a graph as above, node features are extracted using a few base techniques
whose complexity is reasonable for large graphs.

.. _sec-egonet-features:

^^^^^^^^^^^^^^^
Egonet features
^^^^^^^^^^^^^^^

An *egonet* associated with a node (named center) is the subgraph containing the node and all
its direct neighbors. In a directed graph, the egonet is usually defined ignoring
the direction of the edges, but we can define in-egonets or out-egonets by
considering only the nodes having connections only to or from the central node, respectively.

A *reduced egonet*, or *egored*, is the egonet from which the nodes that have a single
connection with the center are removed.

The base features associated with a node, in egonet context, are

	* `'f_degree_in'`: indegree of the node
	* `'f_degree_out'`: outdegree of the node
	* `'f_amount_in'`: total amount received by node from neighbors
	* `'f_amount_out'`: total amount sent by node to neighbors
	* `'f_nr_trans_in'`: number of transactions to the node
	* `'f_nr_trans_out'`: number of transactions from the node
	* `'f_ego_nr_nodes'`: number of nodes in the egonet
	* `'f_ego_nr_edges'`: number of edges in the egonet

Similar features can be defined in the context of an egored:

	* `'f_egored_degree_in'`
	* `'f_egored_degree_out'`
	* `'f_egored_amount_in'`
	* `'f_egored_amount_out'`
	* `'f_egored_nr_trans_in'`
	* `'f_egored_nr_trans_out'`
	* `'f_egored_nr_nodes'`
	* `'f_egored_nr_edges'`

The computation of the base features may take significant time if the graph is large
and so it is recommended to compute them only once.
Notebooks *g2f_ex1-g2f_ex3* show how to do this.

Other egonet or egored features are:

	* `'f_average_amount_in'`: amount-in divided by the number of transactions
	* `'f_average_amount_out'`: same, for amount-out
	* `'f_egored_average_amount_in'`: same as above, but in egored context
	* `'f_egored_average_amount_out'`:
	* `'f_egored_degree_in_rel'`: egored indegree divided by indegree
	* `'f_egored_degree_out_rel'`: same, for outdegree
	* `'f_egored_amount_in_rel'`: egored amount-in divided by amount-in
	* `'f_egored_amount_out_rel'`: same, for amount-out
	* `'f_egored_average_amount_in_rel'`: egored average amount-in divided by average amount-in
	* `'f_egored_average_amount_out_rel'`: same, for amount-out
	* `'f_egored_nr_nodes_rel'`: number of nodes in egored divided by number of nodes in egonet
	* `'f_egored_nr_edges_rel'`: same, for edges
	* `'f_ego_edge_density'`: number of egonet edges divided by number of egonet nodes
	* `'f_egored_edge_density'`: same, for egored

These features can be easily computed when the base features are available.

.. _sec-rwalk-features:

^^^^^^^^^^^^^^^^^^^^
Random walk features
^^^^^^^^^^^^^^^^^^^^

Random walks (RWs) starting from a node and having a given length are generated
and used to obtain the following base features:

    * `'f_rwalk_start_amount'`: amount on the first leg of the RW, averaged over all RWs
    * `'f_rwalk_transfer_out'`: amount transferred all the way from the first node to the last (or minimum amount over all edges of a RW), averaged over all RWs
    * `'f_rwalk_out_back'`: amount transferred back to the starting node, averaged over all RWs
    * `'f_rwalk_out_back_max'`: maximum amount transferred back to the starting node, over all RWs

Note that if a RW returns to the starting node, then it is stopped there,
regardless of its length.
Similar features are computed by generating RWs reversely, from an end node:

    * `'f_rwalk_end_amount'`: amount on the last leg of the RW, averaged over all RWs
    * `'f_rwalk_transfer_in'`: etc.
    * `'f_rwalk_in_back'`:
    * `'f_rwalk_in_back_max'`:

The computation of the above base features may take significant time if the graph is large
and so it is recommended to compute them only once.
Notebooks *g2f_ex1-g2f_ex3* show how to do this.

Other RW features are:

    * `'f_rwalk_ring_max'`: the maximum of `'f_rwalk_out_back_max'` and `'f_rwalk_in_back_max'`
    * `'f_rwalk_ring_average'`: the average of `'f_rwalk_out_back_max'` and `'f_rwalk_in_back_max'`

These features can be easily computed when the base RW features are available.

RWs are computed by default with equal probability for selecting the next node of a walk.
There is also the option to select the next node with a probability that is proportional
with the amounts of the edges towards or from the neighbors.

.. _sec-egonet-spectrum-features:

^^^^^^^^^^^^^^^^^^^^^^^^
Egonet spectrum features
^^^^^^^^^^^^^^^^^^^^^^^^

Egonet spectrum features are vectors of the largest *k* singular values
of Laplacian matrices of the egonet around a node.
The following features are available:

    * `'f_spectrum_Laplacian'`: no edge weights when building the Laplacian, only topologic information is used
    * `'f_spectrum_Laplacian_amount'`: edge weights are the cumulated amounts`
    * `'f_spectrum_Laplacian_average_amount'`: edge weights are the average amount (cumulated amount divided to the number of transactions)
    * `'f_spectrum_Laplacian_n_transactions'`: edge weights are the numbers of transactions

So, the total number of features is the number of given features from the list above
times *k*.
For each type of Laplacian, the singular values are sorted decreasingly.
The computation time grows with the number of features.

.. _sec-transaction-list-to-features:

----------------------------
Transaction list to features
----------------------------

These meaningful features are extracted with respect to the trasaction list, given we have access to the 
user ID (either ordinator/source or beneficiary/destination) and the timestamps of the transactions.

There are four categories of features extracted from the transaction list.

.. _sec-time-related-features:

^^^^^^^^^^^^^^^^^^^^^
Time-related features
^^^^^^^^^^^^^^^^^^^^^

The features extracted directly from the timestamp.
The following features are available:

    * `'day_of_year'`
    * `'day_of_month'`
    * `'day_of_week'`
    * `'year'`
    * `'month'`
    * `'week'`
    * `'hour'`
    * `'minutes_of_hour'`
    * `'minutes_of_day'`

.. _sec-group-historical-time-features:

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Group historical time features
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

From the timestamp, we can compute the time interval from the last transaction of the same user until the current one.
Given we have two users categories (beneficiary or ordinator), there will be two groups to consider:

	* the issued transactions group - where the current client is the ordinator/issuer/source;
	* the received transactions group - where the client is the beneficiary/recipient/destination.

The time difference can be explained in:

    * `'h'` - hours
    * `'m'` - minutes
    * `'s'` - seconds
    * `'D'` - days

.. _sec-group-statistical-features:

^^^^^^^^^^^^^^^^^^^^^^^^^^
Group statistical features
^^^^^^^^^^^^^^^^^^^^^^^^^^

The users categories are kept, same as above (beneficiary or ordinator). 
Based on the amount and other numerical attributes, the user groups and the timestamp,
we can compute statistics over various periods in time, until the current time.

The following periods of time are available:

    * `'1D'` - 1 day
    * `'7D'` - 7 days
    * `'30D'` - 30 days
    * `'60D'` - 60 days
    * `'90D'` - 90 days
    * `'1M'` - 1 month, since the same day of last month (e.g., March 2nd, compared to April 2nd, current time)
    * `'2M'` - 2 months
    * `'3M'` - 3 months

The features available for computation, for a given user, over the most recent period, are:

    * `'max'` - the maximum
    * `'min'` - the minimum
    * `'mean'` - the average
    * `'std'` - the standard deviation
    * `'median'` - median (the 50th percentile)
    * `'quant_0.25'` - the 25th percentile
    * `'quant_0.75'` - the 70th percentile
    * `'count'` - the number of transactions

.. _sec-historical-features:

^^^^^^^^^^^^^^^^^^^
Historical features
^^^^^^^^^^^^^^^^^^^

Historical features represent differences of the current transaction with respect to either
other transactions in time, or a statistical value computed over a most recent period.
The two user groups are considered here as well: beneficiary and ordinator.

For example, we can compute the difference between the amount of the current transaction and the average
over the last 30 days (excluding this day):

    * `'transaction.amount_local - 30day_avg.amount_local'`

.. _sec-transaction-list-to-graph:

-------------------------
Transaction list to graph
-------------------------

Transformation from a transaction list, which is in fact a directed multi-graph,
to a graph as described in :ref:`sec-data-types` is straighforward.
The amounts corresponding to the transactions between an (ordered) pair of nodes (client ids)
are simply added and the transactions are counted.
The directed graph has the cumulated amounts and number of transactions as edge attributes.
Other attributes can be built by addition, similarly to the cumulated amounts.

If other transformations are desired, they should be implemented by the user.
Let us know what transformations could be useful and we will try to implement them.
