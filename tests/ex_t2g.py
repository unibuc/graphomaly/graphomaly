# example of using transaction list to graph methods

import pandas as pd
import transactions_to_graph as t2g

use_matrix = False  # only for testing that the input can also be a matrix

transaction_list_file_name = '../synth_graph_v11_small.pkl'  # file where to find the transaction list
X = pd.read_pickle(transaction_list_file_name)               # input is a dataframe
if use_matrix:
    X = X.to_numpy()
    X = X[:,[2,3,0,1,4]]    # order columns in mandatory order: source id, destination id, amount, others

#print(X)

it2g = t2g.Transactions2Graph()

# transform transaction list to graph
if use_matrix:
    G = it2g.fit_transform(
        X,
        summable_attributes=[3],
        summable_attributes_out_names=["nr_alerts"])
else:
    G = it2g.fit_transform(
        X,
        column_names=["source", "target", "amount"],
        summable_attributes=["label"],
        summable_attributes_out_names=["nr_alerts"])

print(G)
